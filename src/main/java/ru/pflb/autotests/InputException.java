package ru.pflb.autotests;

/**
 * @author Miroshnichenko Anatoliy.
 */
public class InputException extends Exception {
    public InputException() {
    }

    public InputException(String message) {
        super(message);
    }
}
