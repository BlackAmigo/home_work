package ru.pflb.autotests;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Miroshnichenko Anatoliy.
 */
public class DateConversion {

    public String format1(String input) throws InputException {
        DateFormat outputFormat = new SimpleDateFormat("dd/MM/YY");

        return outputFormat.format(inputFormat(input));
    }

    public String format2(String input) throws InputException {
        DateFormat outputFormat = new SimpleDateFormat("YYYY-MM-dd");
        return outputFormat.format(inputFormat(input));
    }

    public String format3(String input) throws InputException {
        DateFormat outputFormat = new SimpleDateFormat("dd_MM");
        return outputFormat.format(inputFormat(input));
    }

    private Date inputFormat(String input) throws InputException {
        DateFormat inputFormat = new SimpleDateFormat("dd-MM-yyyy");
        try {
            Date date = inputFormat.parse(input);
            return date;
        } catch (ParseException | NullPointerException e) {
            throw new InputException("Неверный формат: " + input);
        }
    }

}
