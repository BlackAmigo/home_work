package ru.pflb.autotests;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;

/**
 * @author Miroshnichenko Anatoliy.
 */
public class DateConversionTest {

    private DateConversion dc;

    @BeforeClass
    public void setUp(){
        dc = new DateConversion();
    }

    @DataProvider(name = "dataForEditionFormat1")
    public Object [][] dataForEditionFormat1(){
        return new Object[][]{
                {"02-02-2019","02/02/19"},
                {"10-07-2020","10/07/20"},
                {"08-12-2023","08/12/23"},
        };
    }

    @DataProvider(name = "dataForEditionFormat2")
    public Object [][] dataForEditionFormat2(){
        return new Object[][]{
                {"02-02-2019","2019-02-02"},
                {"10-07-2020","2020-07-10"},
                {"08-12-2023","2023-12-08"},
        };
    }

    @DataProvider(name = "dataForEditionFormat3")
    public Object [][] dataForEditionFormat3(){
        return new Object[][]{
                {"02-02-2019","02_02"},
                {"10-07-2020","10_07"},
                {"08-12-2023","08_12"},
        };
    }

    @Test(dataProvider = "dataForEditionFormat1", expectedExceptions = Exception.class)
    public void format1(String input, String expected) throws Exception {

        String result = dc.format1(input);
        Assert.assertEquals(result , expected);
        throw new Exception();

    }

    @Test(dataProvider = "dataForEditionFormat2")
    public void format2(String input, String expected) throws InputException {
        String result = dc.format2(input);
        Assert.assertEquals(result , expected);
    }

    @Test(dataProvider = "dataForEditionFormat3")
    public void format3(String input, String expected) throws InputException {
        String result = dc.format3(input);
        Assert.assertEquals(result , expected);
    }
}
